<?php

namespace App\Http\Controllers\Back;
use App\Moto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class MotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $motos = Moto::orderBy('updated_at', 'desc')->get();
        return view('back.moto.index', compact('motos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $moto = Moto::create($this->validateRequest());
            $this->storeImage($moto);
            $this->storeVideo($moto);
            return redirect()->back()->with('success', 'Data for new Moto Stored Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Moto $moto)
    {
        return view('back.moto.edit', compact('moto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Moto $moto)
    {        
        $moto->update($this->validateRequest());

        $this->storeImage($moto);

        $this->storeVideo($moto);

        return redirect()->route('moto.create')->with('success', 'Moto Updated Successfully!!!'); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Moto $moto)
    {
        Storage::delete($moto->video);
        Storage::delete($moto->image);
        $moto->delete();
        return redirect()->back()->with('success', 'Moto Deleted Successfully!!!');
    }

    private function validateRequest()
    {
        return request()->validate([
            'title'       => "required|max:100",
            'description' => 'required|min:10',
            'image'       => 'required|sometimes|file|image|max:10000',
            'video'       => 'required|sometimes|file|mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100000',
        ]);
    } 

    private function storeImage($moto)
    {
        if(request()->has('image')) {
            $moto->update([
                'image' => request()->image->store('moto', 'public'),
            ]);
        }
    }

    private function storeVideo($moto)
    {
        if(request()->has('video')) {
            $moto->update([
                'video' => request()->video->store('moto', 'public'),
            ]);
        }
    }
}
