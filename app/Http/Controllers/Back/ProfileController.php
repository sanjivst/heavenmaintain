<?php

namespace App\Http\Controllers\Back;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit(User $user)
    {
        return view('back.admin-profile', compact('user'));
    }

    public function update(Request $request, User $user)
    {
            $user->name = $request->name;
            $user->address = $request->address;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = Hash::make($request->password);
            $user->save();
            // dd($user);
            // $user->update();

            return redirect()->route('profile.edit', compact('user'))->with('success', 'Admin Profile Updated Successfully!!!');
    }

    public function destroy($id)
    {
        //
    }
}
