<?php

namespace App\Http\Controllers\Back;
use App\Subscribe;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class SubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subscribes = Subscribe::orderBy('updated_at', 'desc')->get();
        return view('back.subscribe.index', compact('subscribes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [];

        $Validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|unique:subscribes|email',
            ],
                $messages
        );

        if($Validator->fails()){
            $Response = $Validator->messages();
        } else {
            $subscribe = new Subscribe;
            $subscribe->email = $request->email;
            $subscribe->save();
            $Response = ['success' => 'Your Action has been successfully done.'];
        }
        
        return response()->json($Response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscribe $subscribe)
    {
        $subscribe->delete();
        return redirect()->back()->with('success', 'Email Deleted Successfully!!!');
    }
}
