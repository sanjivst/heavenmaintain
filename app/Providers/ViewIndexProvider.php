<?php

namespace App\Providers;
use App\Team;
use App\Testimonial;
use App\Moto;
use App\User;
use Illuminate\Support\ServiceProvider;

class ViewIndexProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('front.index',function($view)
        {            
            $motos = Moto::orderBy('updated_at', 'desc')->first();
            $view->with(compact('motos'));
        });
        
        view()->composer('front.index',function($view)
        {            
            $testimonials = Testimonial::orderBy('updated_at', 'desc')->get();
            $view->with(compact('testimonials'));
        });

        view()->composer('front.index', function($view)        
        {
            $teams = Team::orderBy('updated_at', 'desc')->get();
            $view->with(compact('teams'));
        });

        view()->composer('back.index',function($view)
        {            
            $user = User::orderBy('updated_at', 'desc')->first();
            $view->with(compact('user'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
