<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

//Frontend Routes
Auth::routes();

//Contact Page
Route::get('/contact', 'HomeController@contact')->name('contact');

//Home Page
Route::get('/', function () {
    return view('front.index');
});
//Store Guest Via JoinUs
Route::post('/admin/store-joinus', 'Back\JoinusController@store')->name('joinus.store');

//Store Email of Guest For Newsletter
Route::post('/admin/store-subscribe', 'Back\SubscribeController@store')->name('subscribe.store');

//Backend routes
Route::middleware(['auth'])->group(function () {

    Route::get('/admin', function () {
        return view('back.index');
    });

    //Moto
    Route::get('/admin/create-moto', 'Back\MotoController@create')->name('moto.create');
    Route::post('/admin/store-moto', 'Back\MotoController@store')->name('moto.store');
    Route::get('/admin/edit-moto/{moto}', 'Back\MotoController@edit')->name('moto.edit');
    Route::patch('/admin/update-moto/{moto}', 'Back\MotoController@update')->name('moto.update');
    Route::get('/admin/destroy-moto/{moto}', 'Back\MotoController@destroy')->name('moto.destroy');


    //Testimonial
    Route::get('/admin/create-testimonial', 'Back\TestimonialController@create')->name('testimonial.create');
    Route::post('/admin/store-testimonial', 'Back\TestimonialController@store')->name('testimonial.store');
    Route::get('/admin/edit-testimonial/{testimonial}', 'Back\TestimonialController@edit')->name('testimonial.edit');
    Route::patch('/admin/update-testimonial/{testimonial}', 'Back\TestimonialController@update')->name('testimonial.update');
    Route::get('/admin/destroy-testimonial/{testimonial}', 'Back\TestimonialController@destroy')->name('testimonial.destroy');


    //Team
    Route::get('/admin/create-team', 'Back\TeamController@create')->name('team.create');
    Route::post('/admin/store-team', 'Back\TeamController@store')->name('team.store');
    Route::get('/admin/edit-team/{team}', 'Back\TeamController@edit')->name('team.edit');
    Route::patch('/admin/update-team/{team}', 'Back\TeamController@update')->name('team.update');
    Route::get('/admin/destroy-team/{team}', 'Back\TeamController@destroy')->name('team.destroy');


    //Subscribe
    Route::get('/admin/create-subscribe', 'Back\SubscribeController@create')->name('subscribe.create');
    Route::get('/admin/destroy-subscribe/{subscribe}', 'Back\SubscribeController@destroy')->name('subscribe.destroy');

    //Join Us
    Route::get('/admin/destroy-joinus/{joinus}', 'Back\JoinusController@destroy')->name('joinus.destroy');
    Route::get('/admin/show-joinus', 'Back\JoinusController@show')->name('joinus.show');

    //Admin-Profile
    Route::get('/admin/edit-profile/{user}', 'Back\ProfileController@edit')->name('profile.edit');
    Route::patch('/admin/update-profile/{user}', 'Back\ProfileController@update')->name('profile.update');

});

