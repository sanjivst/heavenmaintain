</div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{asset('Back/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('Back/dist/js/jquery.ui.touch-punch-improved.js')}}"></script>
    <script src="{{asset('Back/dist/js/jquery-ui.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('/Back/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('Back/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('Back/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('Back/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('Back/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('Back/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('Back/dist/js/custom.min.js')}}"></script>
    <!-- this page js -->
    <script src="{{asset('Back/assets/libs/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('Back/assets/libs/fullcalendar/dist/fullcalendar.min.js')}}"></script>
    <script src="{{asset('Back/dist/js/pages/calendar/cal-init.js')}}"></script>

</body>

</html>