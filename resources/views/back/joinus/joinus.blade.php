
@extends('back.include.layout')
@section('content')
    
<div class="container">
    <div id="join-us">
    <h2 class="text-center">List of Guest</h2>
    <table class="table mt-5">
        <thead>
        <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Contact</th>
            <th scope="col">Address</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @php($i = 1)
        @foreach($joinuses as $joinus)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$joinus->full_name}}</td>
                <td>{{$joinus->email}}</td>
                <td>{{$joinus->contact}}</td>
                <td>{{$joinus->address}}</td>
                <td>{{$joinus->description}}</td>

                <td>
                    <a href = "{{route('joinus.destroy', $joinus->id)}}" class = "btn btn-danger btn-sm">Delete</a>

                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    </div>
</div>
@stop

