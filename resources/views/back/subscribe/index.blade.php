@extends('back.include.layout')
@section('content')


<div class = "container">

  <div id="newsletter">
    <h2 class="text-center">List of Email</h2>
    <table class="table mt-5">
            <thead>
              <tr>
                <th scope="col">S.N.</th>
                <th scope="col"> Email</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @php($i = 1)
              @foreach($subscribes as $subscribe)
              <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$subscribe->email}}</td>

                <td>
                  <a href = "{{route('subscribe.destroy', $subscribe->id)}}" class = "btn btn-danger btn-sm">Delete</a>

                </td>
              </tr>
              @endforeach
              
            </tbody>
    </table>
  </div>
</div>
@stop