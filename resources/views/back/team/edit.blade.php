@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    <h2>Edit Team Member</h2>
{{Form::open(['method'=>'patch', 'route'=>['team.update',$team->id], 'enctype' => 'multipart/form-data'])}}
<div class = "form-group">
    <label>Name</label>
    <input type="string"  name='name' value = "{{ old('name') ?? $team->name }}" class="form-control @error('name') is-invalid @enderror" required><br>
    @error('name')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class = "form-group">
    <label>Post and Company Name</label>
    <input type="string"  name='post_with_company' value="{{ old('post_with_company') ?? $team->post_with_company}}" class="form-control @error('post_with_company') is-invalid @enderror" required><br>
    @error('post_with_company')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class = "form-group">
    <label>Image</label>
    <input type="file"  name='image' class="form-control @error('image') is-invalid @enderror">{{ old('image') ?? $team->image}}<br>
    @error('image')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class = "form-group">
    <label>Description</label>
    <textarea name='description' class="form-control @error('description') is-invalid @enderror" required>{{ old('description') ?? $team->description}}</textarea>
    @error('description')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}
</div>

@stop
