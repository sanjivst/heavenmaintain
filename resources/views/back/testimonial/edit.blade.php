@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    <h2>Edit Testimonial</h2>
    {{Form::open(['method'=>'patch', 'route'=>['testimonial.update', $testimonial->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    <div class = "form-group">
        <label>Employee Name</label>
        <input type="string"  name='name' value = "{{ old('name') ?? $testimonial->name }}" class="form-control @error('name') is-invalid @enderror" required><br>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class = "form-group">
        <label>Post</label>
        <input type="string"  name='post' value = "{{ old('post') ?? $testimonial->post }}" class="form-control @error('post') is-invalid @enderror" required><br>
        @error('post')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    </div>
    <div class = "form-group">
        <label>Image</label>
        <input type="file"  name='image' value="{{ old('image') ?? $testimonial->image }}" class="form-control @error('image') is-invalid @enderror">{{ $testimonial->image }}<br>
        @error('image')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class = "form-group">
        <label>Description</label>
        <textarea name='description' class="form-control @error('description') is-invalid @enderror" required>{{ old('description') ?? $testimonial->description }}</textarea>
        @error('description')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>
@stop
