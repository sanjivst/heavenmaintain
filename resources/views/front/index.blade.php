@include('front.include.header')
	<div id="main-wrapper">
		<section class="section top-header-section">
			<div class="overlay-header"></div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="logo-head">
							<a href="/.">
								<img src="front/images/frontend/logo-hm-white-03.png" class="img-fluid text-center">
							</a>
						</div>
		            </div>
		            <div class="col-lg-12">
			            <div class="select-service-wrapper">
			            	<h4 class="select-service-title">Select Service</h4>
			            	<div class="select-service-items-wrapper">
			            		<div class="select-service-items">
			            			<div class="service-img">
			            				<a href="https://www.alpasal.com/">
			            					<img src="front/images/frontend/service-logo-03.png" class="img-fluid">
			            				</a>
			            			</div>
			            			<div class="service-name">
			            				<a href="https://www.alpasal.com/">Alpasal</a>
			            			</div>
			            		</div>
			            		<div class="select-service-items">
			            			<div class="service-img">
			            				<a href="https://www.alkrishi.com/">
			            					<img src="front/images/frontend/service-logo-01.png" class="img-fluid">
			            				</a>
			            			</div>
			            			<div class="service-name">
			            				<a href="https://www.alkrishi.com/">Alkrishi</a>
			            			</div>
			            		</div>
			            		<div class="select-service-items">
			            			<div class="service-img">
			            				<a href="http://omlotexpress.com/">
				            				<img src="front/images/frontend/service-logo-02.png" class="img-fluid" >
				            			</a>
			            			</div>
			            			<div class="service-name">
			            				<a href="http://omlotexpress.com/">Omlot Express</a>
			            			</div>
			            		</div>
			            		<div class="select-service-items">
			            			<div class="service-img">
			            				<a href="http://omlotstate.com/">
				            				<img src="front/images/frontend/service-logo-04.png" class="img-fluid">
				            			</a>
			            			</div>
			            			<div class="service-name">
			            				<a href="http://omlotstate.com/">Omlot State</a>
			            			</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			        <div class="col-lg-12 d-flex justify-content-center">
			        	<div class="header-navi">
			        		<ul class="header-ul">
			        			<!-- <li>
			        				<a href="{{ route('contact') }}">
			        					<i class="fas fa-mobile-alt"></i>
			        					Contact Us
			        				</a>
			        			</li> -->
			        			<li>
			        				<a href="#" data-toggle="modal" data-target="#join-us-modal">
			        					<i class="fas fa-handshake"></i>
			        					Join Us
			        				</a>
			        			</li>
			        		</ul>
			        	</div>
			        </div>
				</div>
			</div>
		</section>

		<!-- Moto Section Starts Here -->
		@if(!empty($motos))
		<section class="section index-first-sec">
			<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="index-desc">
								<h3>Our moto is ...</h3>
								<h4>
									{{$motos->title}}
								</h4>
								<p class="desc-p">
									{{$motos->description}}
								</p>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="video-wrapper">
								<div class="first-sec-overlay"></div>
								<img src="{{ asset('storage/' . $motos->image) }}" class="img-fluid" alt="Image">
								<!-- <span class="shadow"></span> -->
								<button type="button" data-toggle="modal" data-target="#videoModal">
									<div class="play-button"><i class="fas fa-play"></i></div>
								</button>
							</div>
						</div>
					</div>
			</div>
			<!-- video modal starts -->
			<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
	            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	                <div class="modal-content">
	                    <div class="modal-body custom-close-video">
	                    	<button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
					          	<span aria-hidden="true">&times;</span>
					        </button>
	                        <video class="video" width="100%" controls>
	                            <source src="{{ asset('storage/' . $motos->video) }}" type="video/mp4">
	                        </video>
	                    </div>
	                </div>
	            </div>
	        </div>
			<!-- video modal ends -->
		</section>
		@endif
		<!-- Moto Section ends here -->

		<!-- Team section starts here -->
		@if(!$teams->isEmpty())
		<section class="section index-fourth-sec">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 p-0">
						<div class="testimonial-wrapper">
							<div class="testimonial-title">
							 	<h4>Our Team</h4>
							</div>

                            <div class="testimonial-main">
                                <div class="testimonial-bg owl-carousel">
                                    @foreach($teams as $team)
                                        <div class="item">
                                            <div class="testimonial-content">
                                                <div class="quote-circle">
                                                    <i class="fas fa-quote-right"></i>
                                                </div>
                                                <p class="testi-content-p">
                                                    {{$team->description}}
                                                </p>
                                                <div class="quote-footer">
                                                    <i class="fas fa-quote-left"></i>
                                                </div>
                                                <div class="testimonial-author">
                                                    <h4>
                                                        {{$team->name}}
                                                    </h4>
                                                    <p>
                                                        {{$team->post_with_company}}
                                                    </p>
                                                </div>
                                            </div>
                                            <img src="{{ asset('storage/' . $team->image) }}" alt="photo" class="img-fluid">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
						</div>
					</div>
				</div>
            </div>
        </section>
		@endif
		<!-- Team section ends here -->

		<!-- Testimonial Section Starts here -->
		@if(!$testimonials->isEmpty())
		<section class="section index-third-sec">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="team-wrapper">
							<div class="team-title">
							 	<h4>Our Testimonial</h4>
							</div>
							<div class="team-wrap-div">
								<ul class="owl-carousel">
								@foreach($testimonials as $testimonial)

									<li class="team-item">
										<i class="fas fa-quote-left"></i>

										<p class="team-item-des-p">
    										{{ $testimonial->description }}
										</p>
										<div class="team-info">
											<div class="team-item-img">
													<img src="{{ asset('storage/' . $testimonial->image) }}" class="img-fluid" alt="">
											</div>
											<div class="team-info-h">
												<h4>
                                                    {{$testimonial->name}}
												</h4>

												<h5>
												    {{$testimonial->post}}
												</h5>
											</div>
										</div>
                                    </li>
									@endforeach
                                </ul>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</section>
		@endif
		<!-- Testimonial section ends here -->

		<!-- Newsletter Sections Starts here -->
		<section class="section index-fifth-sec">
			<div class="newsletter-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="newsletter-wrapper">
							<div class="newsletter-div-main row">
								<div class="col-lg-6 col-lg-offset-3">
									<form id="newsletter-form" data-route="{{ route('subscribe.store') }}" method ="post" class="newletter-form-index">
										@csrf
										<div class="input-group">
										  	<div class="input-group-prepend">
										    	<span class="input-group-text" id="inputGroup-sizing-default">
										    		<i class="far fa-envelope"></i>
										    	</span>
										  	</div>
											<input type="email" value="{{ old('email') }}" class="form-control" name = "email" placeholder="Email" aria-label="email" aria-describedby="">
											<div class="input-group-append">
										    	<button class="btn btn-outline-secondary" type="submit">
										    		Subscribe
										    	</button>
										  	</div>
										</div>
									</form>
								</div>
								<div class="col-lg-6 col-lg-offset-3">
									<p class="newsletter-title-p">
										Enter your email for subscribe to get monthly Newsletter
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Newsletter Section ends here -->


		<!-- Footer Section  -->
		<section class="section-footer">
			<footer class="footer">
				<div class="container">
					<div class="row ">
						<div class="col-lg-8">
							<div class="footer-service-wrapper">
								<div class="service-wrapper">
								<h3 class="select-service-title text-left">Select Service</h3>
									<div class="row">
										<div class="col-sm-6">
											<div class="service-img">
												<a href="https://www.alpasal.com/" target="_blank">
													<img src="front/images/frontend/service-logo-03.png" class="img-fluid">
												</a>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="service-img">
												<a href="https://www.alkrishi.com/" target="_blank">
													<img src="front/images/frontend/service-logo-01.png" class="img-fluid">
												</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="service-img">
												<a href="https://www.omlotexpress.com/" target="_blank">
													<img src="front/images/frontend/service-logo-02.png" class="img-fluid" >
												</a>
											</div>
										</div>
										<div class="col-sm-6">	
											<div class="service-img">
												<a href="https://www.omlotstate.com/" target="_blank">
													<img src="front/images/frontend/service-logo-04.png" class="img-fluid">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="footer-contact-wrapper">
								<div class="contact-wrapper team-title text-right">
								<h3>Contact Us</h3> 
									<ul>
										<li>
											New Baneshwor, Kathmandu, NEPAL
										</li>
										<li>
											<i class="fas fa-phone"></i>
											<a href="tel:+977 14780294" class="text-dark" data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Call">
												0-1-4780-294
											</a>
										</li>
										<li>
											<i class="fas fa-envelope"></i>
											<a href="mailto:info@heavenmaker.com" class="text-dark" data-toggle="tooltip" data-html="true" data-placement="bottom" title="Send Mail">
												info@heavenmaker.com
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="copyright-section">
					<p class="text-center pt-4">
						Copyright © <script>document.write(new Date().getFullYear())</script> 
						<a href="https://www.heavenmaker.org" class="text-dark">Heaven Maker Pvt. Ltd.</a>
					</p>
				</div>				
    		</footer>
		</section>

		<!-- join us modal div starts -->

		<div class="modal fade" id="join-us-modal" tabindex="-1" role="dialog" aria-labelledby="join-us-modalTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
			    <div class="modal-content" id="create">
				    <div class="modal-header modal-bg-grad">
				        <h5 class="modal-title" id="join-us-modal">Join Us</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          	<span aria-hidden="true">&times;</span>
				        </button>
				    </div>
                    <div id="success"></div>
                    <form id="join-us-form" data-route="{{ route('joinus.store') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="get-touch-wrapper">
                                        <!-- <h3>Join Us</h3> -->
                                        <div class="get-touch-main">
                                                <div id="full_name" class="form-group col-lg-12">
                                                    <label for="full_name">Full Name</label>
                                                    <input type="text" name="full_name" value="{{ old('full_name') }}" class="form-control">
                                                </div>
                                                <div id="email" class="form-group col-lg-12">
                                                    <label for="email">Email</label>
                                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                                                </div>
                                                <div id="contact" class="form-group col-lg-12">
                                                    <label for="contact">Phone Number</label>
                                                    <input type="number" name="contact" value="{{ old('contact') }}" class="form-control">
                                                </div>
                                                <div id="address" class="form-group col-lg-12">
                                                    <label for="address">Address</label>
                                                    <input type="text" name="address" value="{{ old('address') }}" class="form-control">
                                                </div>
                                                <div id="description" class="form-group col-lg-12">
                                                    <label for="description">Description</label>
                                                    <textarea name="description" class="form-control" id="" cols="10" rows="5">{{ old('description') }}</textarea>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
				        </div>
				    <div class="modal-footer">
				        <button type="submit" class="btn btn-primary btn-modal-sub">Submit</button>
				    </div>
                    </form>
			    </div>

            </div>
		</div>
		<!-- join us modal div ends  -->
	</div>
@include('front.include.footer')
