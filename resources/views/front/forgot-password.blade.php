<!DOCTYPE html>
<html lang="en"> 
<head>
<title>Heaven Maker</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link rel="icon" type="image/png" href="images/icons/favicon.ico" /> -->

<link href="front/css/bootstrap.min.cs'}}s" rel="stylesheet">      
<link rel="stylesheet" type="text/css" href="front/css/select2.min.cs'}}s"> 
<link rel="stylesheet" type="text/css" href="front/css/style.css">

</head>
<body>
<div class="limiter">
	<div class="container-login" style="background-image: url('front/images/img-01.jpg');">
		<div class="wrap-login">
			<form class="login-form ">
				<!-- <div class="login-form-avatar">
					<img src="images/logo-light-icon.png" alt="AVATAR">
				</div> -->
				<span class="login-form-title">
					Change Password
				</span> 
				<div class="wrap-input m-b-10">
					<input class="input" type="password" name="pass" placeholder="New Password">
					<span class="focus-input"></span>
					<span class="symbol-input">
						<i class="fa fa-lock"></i>
					</span>
				</div>
				<div class="wrap-input m-b-10">
					<input class="input" type="password" name="pass" placeholder="Confirm Password">
					<span class="focus-input"></span>
					<span class="symbol-input">
						<i class="fa fa-lock"></i>
					</span>
				</div>
				<div class="container-login-form-btn">
					<button class="login-form-btn">
						Change Password
					</button>
				</div>
				<div class="text-center m-t-10">
					<a href="login.php" class="txt1">
						Login
					</a>
				</div> 
			</form>
		</div>
	</div>
</div>

<script src="{{asset('front/js/jquery.min.js')}}"></script> 
<script src="{{asset('front/js/popper.min.js')}}"></script>
<script src="{{asset('front/js/bootstrap.min.js')}}"></script> 
<script src="{{asset('front/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('front/js/select2.full.min.js')}}"></script>
<script src="{{asset('front/js/style.js')}}" type="text/javascript"></script>
 
 
</html>
