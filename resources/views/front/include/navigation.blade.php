<aside class="left-sidebar"> 
            <div class="scroll-sidebar"> 
                <nav class="sidebar-nav">
                    <ul id="sidebarnav"> 
                        <li>
                            <a href="index.php" aria-expanded="false">
                                <i class="mdi mdi-gauge"></i>
                                <span class="hide-menu">Dashboard </span>
                            </a> 
                        </li>
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false">
                                <i class="mdi mdi-bullseye"></i>
                                <span class="hide-menu">User</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="add-user.php">Add User</a></li> 
                                <li><a href="user-list.php">User List</a></li> 
                            </ul>
                        </li> 
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false">
                                <i class="mdi mdi-bullseye"></i>
                                <span class="hide-menu">Department</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="add-department.php">Add Department</a></li> 
                                <li><a href="department-list.php">Department List</a></li> 
                            </ul>
                        </li> 
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false">
                                <i class="mdi mdi-bullseye"></i>
                                <span class="hide-menu">Task</span>
                            </a>
                            <ul aria-expanded="false" class="collapse"> 
                                <li><a href="add-specific.php">Add Specific Task</a></li>  
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false">
                                <i class="mdi mdi-bullseye"></i>
                                <span class="hide-menu">Announcement</span>
                            </a>
                            <ul aria-expanded="false" class="collapse"> 
                                <li><a href="add-announcement.php">Add Announcement</a></li>  
                            </ul>
                        </li> 
                    </ul>
                </nav> 
            </div> 
        </aside>