<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="Heaven Maker Group Website for all">
   <meta name="author" content="Heaven Maker Group"> 
   <link rel="apple-touch-icon" sizes="57x57" href="front/images/frontend/favicons/apple-icon-57x57.png">
   <link rel="apple-touch-icon" sizes="60x60" href="front/images/frontend/favicons/apple-icon-60x60.png">
   <link rel="apple-touch-icon" sizes="72x72" href="front/images/frontend/favicons/apple-icon-72x72.png">
   <link rel="apple-touch-icon" sizes="76x76" href="front/images/frontend/favicons/apple-icon-76x76.png">
   <link rel="apple-touch-icon" sizes="114x114" href="front/images/frontend/favicons/apple-icon-114x114.png">
   <link rel="apple-touch-icon" sizes="120x120" href="front/images/frontend/favicons/apple-icon-120x120.png">
   <link rel="apple-touch-icon" sizes="144x144" href="front/images/frontend/favicons/apple-icon-144x144.png">
   <link rel="apple-touch-icon" sizes="152x152" href="front/images/frontend/favicons/apple-icon-152x152.png">
   <link rel="apple-touch-icon" sizes="180x180" href="front/images/frontend/favicons/apple-icon-180x180.png">
   <link rel="icon" type="image/png" sizes="192x192"  href="front/images/frontend/favicons/android-icon-192x192.png">
   <link rel="icon" type="image/png" sizes="32x32" href="front/images/frontend/favicons/favicon-32x32.png">
   <link rel="icon" type="image/png" sizes="96x96" href="front/images/frontend/favicons/favicon-96x96.png">
   <link rel="icon" type="image/png" sizes="16x16" href="front/images/frontend/favicons/favicon-16x16.png">
   <link rel="manifest" href="front/images/frontend/favicons/manifest.json">
   <meta name="msapplication-TileColor" content="#ffffff">
   <meta name="msapplication-TileImage" content="front/images/frontend/fms-icon-144x144.png">
   <meta name="theme-color" content="#ffffff">
   <title>Heaven Maker</title> 
   <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet"> 
   <link href="{{asset('front/css/summernote.css')}}" rel="stylesheet" />  
   <link href="{{asset('front/css/select2.min.css')}}" rel="stylesheet"> 
   <link href="{{asset('front/css/owl-carousel.min.css')}}" rel="stylesheet"> 
   <link href="{{asset('front/css/style.css')}}" rel="stylesheet">     
</head>
<body>    